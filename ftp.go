package main

import (
	"bufio"
	"compress/gzip"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
)

func getLatestFile() (string, error) {
	files, err := ioutil.ReadDir(".")
	if err != nil {
		return "", err
	}

	r, err := regexp.Compile("^.+\\.gz$")
	if err != nil {
		return "", err
	}

	result := ""
	for _, f := range files {
		name := f.Name()
		if r.MatchString(name) {
			result = name
		}
	}
	return result, nil
}

func getFilesList(url string) ([]string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	r, err := regexp.Compile("<a href=\"(.+\\.gz)\">")
	if err != nil {
		return nil, err
	}

	matches := r.FindAllStringSubmatch(string(body), -1)
	var result []string
	for _, match := range matches {
		result = append(result, match[1])
	}

	return result, nil
}

func downloadFile(path string, url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	out, err := os.Create(path)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	return err
}

func uncompressFile(inputPath string, outputPath string) error {
	inputFile, err := os.Open(inputPath)
	if err != nil {
		return err
	}
	defer inputFile.Close()

	outputFile, err := os.Create(outputPath)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	reader := bufio.NewReader(inputFile)
	in, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	defer in.Close()

	writer := bufio.NewWriter(outputFile)
	_, err = io.Copy(writer, in)
	if err != nil {
		return err
	}

	return writer.Flush()
}
