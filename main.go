package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/smtp"
	"os"
	"sort"
	"strings"
)

// AbstractWithScore is an abstract with a score
type AbstractWithScore struct {
	abstract string
	score    float64
}

var pubMedURL = flag.String("url", "https://ftp.ncbi.nlm.nih.gov/pubmed/updatefiles/", "address of update files")
var testArticlePath = flag.String("test-article", "test_article.txt", "path to test article")

var smptServer = flag.String("smtp-server", "smtp.gmail.com", "smtp server")
var smtpPort = flag.Int("smtp-port", 587, "smtp port")
var senderEmail = flag.String("sender-email", "epicurfromsamos@gmail.com", "email of the sender")
var receiverEmail = flag.String("receiver-email", "zimny.mikolaj@gmail.com", "email of the receiver")
var emailPassword = flag.String("email-password", "qwe123a!", "password for sender's email account")

func sendEmail(subject string, content string) error {
	addr := fmt.Sprintf("%s:%d", *smptServer, *smtpPort)
	msg := fmt.Sprintf("From: %s\nTo: %s\nSubject: %s\n\n%s", *senderEmail, *receiverEmail, subject, content)

	auth := smtp.PlainAuth("", *senderEmail, *emailPassword, *smptServer)
	return smtp.SendMail(addr, auth, *senderEmail, []string{*receiverEmail}, []byte(msg))
}

func sortSimilarities(abstracts []string, similarities []float64) []AbstractWithScore {
	var result []AbstractWithScore
	for i, abstract := range abstracts {
		result = append(result, AbstractWithScore{abstract, similarities[i]})
	}
	sort.Slice(result, func(i, j int) bool {
		return result[i].score > result[j].score
	})
	return result
}

func getFilesToDownload(latestFile string, listOfFiles []string) []string {
	sort.Strings(listOfFiles)
	if latestFile == "" {
		return []string{listOfFiles[len(listOfFiles)-1]}
	}

	var result []string
	foundLatest := false
	for _, file := range listOfFiles {
		if foundLatest {
			result = append(result, file)
		} else if file == latestFile {
			foundLatest = true
		}
	}
	return result
}

func getAbstracts(pubMedFile string, testArticle string, abstracts []string) []string {
	fileURL := fmt.Sprintf("%s%s", *pubMedURL, pubMedFile)
	fmt.Printf("Downloading %s...\n", fileURL)
	err := downloadFile(pubMedFile, fileURL)
	if err != nil {
		log.Fatalf("Failed to download updates file: %v", err)
	}

	fmt.Println("Parsing articles...")
	articles, err := parseCompressedArticles(pubMedFile)
	if err != nil {
		log.Fatalf("Failed to parse articles: %v", err)
	}
	fmt.Printf("Loaded %d articles.\n", len(articles))

	for _, article := range articles {
		text := strings.Join(article.MedlineCitation.Article.Abstract.AbstractText, " ")
		abstracts = append(abstracts, text)
	}
	return abstracts
}

func main() {
	flag.Parse()

	testArticleBytes, err := ioutil.ReadFile(*testArticlePath)
	if err != nil {
		log.Fatalf("Failed to read test article: %v", err)
	}
	testArticle := string(testArticleBytes)

	latestFile, err := getLatestFile()
	if err != nil {
		log.Fatalf("Failed to find latest file: %v", err)
	}
	fmt.Printf("Latest file: %s\n", latestFile)

	fmt.Println("Getting list of files...")
	list, err := getFilesList(*pubMedURL)
	if err != nil {
		log.Fatalf("Failed to get files list: %v", err)
	}

	toDownload := getFilesToDownload(latestFile, list)
	if len(toDownload) == 0 {
		fmt.Println("No new files.")
		os.Exit(0)
	}

	var abstracts []string
	for _, file := range toDownload {
		abstracts = getAbstracts(file, testArticle, abstracts)
	}

	corpus := newCorpus()
	testCounts := corpus.addDocument(testArticle)
	var abstractCounts []map[int]int
	for _, abstract := range abstracts {
		counts := corpus.addDocument(abstract)
		abstractCounts = append(abstractCounts, counts)
	}

	testTfidf := corpus.tfidf(testCounts)
	var similarities []float64
	for _, counts := range abstractCounts {
		abstractTfidf := corpus.tfidf(counts)
		similarity := corpus.cosineSimilarity(testTfidf, abstractTfidf)
		similarities = append(similarities, similarity)
	}

	bestArticles := sortSimilarities(abstracts, similarities)
	for i := 0; i < 0; i++ {
		log.Println(bestArticles[i])
	}

	if err := sendEmail(fmt.Sprintf("%f", bestArticles[0].score), bestArticles[0].abstract); err != nil {
		log.Fatalf("Failed to send email: %v", err)
	}
}
