package main

import (
	"bufio"
	"encoding/xml"
	"os"
)

// PubmedArticleSet is a set of articles
type PubmedArticleSet struct {
	PubmedArticles []PubmedArticle `xml:"PubmedArticle"`
}

// PubmedArticle format: https://www.nlm.nih.gov/bsd/licensee/elements_descriptions.html
type PubmedArticle struct {
	MedlineCitation struct {
		Article struct {
			Abstract struct {
				AbstractText []string `xml:"AbstractText"`
			} `xml:"Abstract"`
		} `xml:"Article"`
	} `xml:"MedlineCitation"`
}

func parseCompressedArticles(compressedPath string) ([]PubmedArticle, error) {
	uncompressedPath := compressedPath[:len(compressedPath)-3]
	err := uncompressFile(compressedPath, uncompressedPath)
	if err != nil {
		return nil, err
	}

	file, err := os.Open(uncompressedPath)
	if err != nil {
		return nil, err
	}
	defer os.Remove(uncompressedPath)
	defer file.Close()

	reader := bufio.NewReader(file)
	decoder := xml.NewDecoder(reader)

	var result []PubmedArticle
	for {
		t, _ := decoder.Token()
		if t == nil {
			break
		}

		switch element := t.(type) {
		case xml.StartElement:
			name := element.Name.Local
			if name == "PubmedArticle" {
				var pubmedArticle PubmedArticle
				err := decoder.DecodeElement(&pubmedArticle, &element)
				if err != nil {
					return nil, err
				}
				result = append(result, pubmedArticle)
			}
		}
	}

	return result, nil
}
